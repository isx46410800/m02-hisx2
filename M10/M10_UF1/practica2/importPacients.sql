--PRACTICA 2

--CRON
psql -d lab_clinic -U postgres -f importPacients.sql > errorsimportPacients.data.hora


--conectem a la bbdd de clinic
\c lab_clinic

--creem una taula de nous_pacients
CREATE TABLE nous_pacients AS
SELECT *
FROM   pacients
LIMIT  0;

--Add the bigserial column:
ALTER TABLE nous_pacients ADD column id_pacient_nou bigserial;

--INSERT data from old table, automatically filling the bigserial:
INSERT INTO nous_pacients
SELECT *    
FROM  pacients

--copiem els pacients nous que estan a /tmp
\COPY pacients_nous FROM '/tmp/nouspacients.csv' DELIMETER ',';
select mirar_pacients();

--crida a una funcio
CREATE OR REPLACE FUNCTION mirar_pacients() 
RETURNS  AS 
$$
DECLARE

BEGIN
	perform * from pacients_nous where 
	if found
		update
	else
		insert
	end if;
END;
$$
LANGUAGE 'plpgsql';















