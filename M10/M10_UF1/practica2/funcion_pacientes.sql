--FUNCION PARA MIRAR PACIENTES NUEVOS

CREATE OR REPLACE FUNCTION mirar_pacients() 
RETURNS text AS 
$$
DECLARE
	registre_actual record;
	strConsulta text;
	recompteDNIs integer;
	resultat text;
	es_dni_correcte boolean := True;
BEGIN
	strConsulta := 'select * from pacients_nous';
	
	FOR registre_actual IN EXECUTE(strConsulta) LOOP
		select count (*) into recompteDNIs from pacients
		where dni=registre_actual.dni;
		
		if recompteDNIs > 0 then
		
			update table pacients;
		else
			if es_dni_correcte(registre_actual.dni) then
			insert into pacients (idpacient, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email) 
	values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11); 
			else
				resultat := 'DNI ' || registre_actual.dni || ' Incorrecte'
			end if;
			
		end if;
	
	
	
	END LOOP;
	
END;
$$
LANGUAGE 'plpgsql';



----
CREATE OR REPLACE FUNCTION pri_sel(id_cli integer)
RETURNS text AS
$$
DECLARE
 	result text := '';
 	searchsql text := '';
 	res_select record;
BEGIN
 	searchsql := 'SELECT * FROM pacients_nous';
  
	 FOR res_select IN EXECUTE(searchsql) LOOP
		 IF result > '' THEN
 			result := result || ';' || res_select.num_clie || '= ' || res_select;
		 ELSE
 			result := res_select.num_clie || '= ' ||res_select;
		 END IF;
 	END LOOP;
 	IF result = '' THEN
 		result := 'Dades inexistents';
	END IF;

   	RETURN searchsql || ': ' || result;
EXCEPTION 
  	WHEN others THEN return '5';
END;
$$
