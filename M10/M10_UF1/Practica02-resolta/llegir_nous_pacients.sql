CREATE OR REPLACE FUNCTION llegir_nous_pacients() 
RETURNS text AS

--3.3.1- Llegirà la taula pacients_nous
--3.3.2- Per cada pacient nou es mirarà si ja existeix a la taula pacients 
--3.3.3- Si el pacient no existeix s'insertarà 
--3.3.4- Si el pacient ja existeix es modificarà posant les dades noves.
--3.3.5- Esborrar el pacient acabat d'insertar de la taula de pacients nous

--Utilitza la funció es_dni_correcte(text) que retorna booleà

$$
DECLARE
ErrorsStr text :='';
Resultat text := '';
selectnouspacients text;
selectexisteixpacient text;
numreg integer:=0;
resultatCercaDNI integer;

reg_resultatsql record;
BEGIN

selectnouspacients:='SELECT * FROM pacients_nous';

--'Iterar per cada registre de la taula de pacients_nous:'
FOR reg_resultatsql IN EXECUTE (selectnouspacients) LOOP
    numreg := numreg + 1;
--    'per saber si un pacient existeix, comprovem només per DNI'
	--Comptem quants DNI com el d'entrada hi ha:
    SELECT count(*) into resultatCercaDNI FROM pacients WHERE dni ILIKE reg_resultatsql.dni;

    RAISE INFO 'S''han recomptat % DNIs com el %', resultatCercaDNI, reg_resultatsql.dni;

    IF resultatCercaDNI> 0 THEN
        --podem suposar que el DNI és correcte, tot i que podria ser molt suposar
        UPDATE pacients SET nom=reg_resultatsql.nom, cognoms=reg_resultatsql.cognoms, data_naix=reg_resultatsql.data_naix, 
            sexe=reg_resultatsql.sexe, adreca=reg_resultatsql.adreca, ciutat=reg_resultatsql.ciutat, c_postal=reg_resultatsql.c_postal,
            telefon=reg_resultatsql.telefon, email=reg_resultatsql.email, num_ss=reg_resultatsql.num_ss, num_cat=reg_resultatsql.num_cat,
            nie=reg_resultatsql.nie, passaport=reg_resultatsql.passaport
            WHERE dni=reg_resultatsql.dni;
        RAISE INFO 'Actualitzades les dades de la pacient amb DNI %', reg_resultatsql.dni;
            
    ELSE
        --fer un insert si el DNI del nou és correcte
        IF es_dni_correcte(reg_resultatsql.dni) THEN
			INSERT INTO pacients (idpacient, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email, num_ss, num_cat, nie, passaport)
			VALUES (DEFAULT, reg_resultatsql.nom, reg_resultatsql.cognoms, reg_resultatsql.dni, reg_resultatsql.data_naix, reg_resultatsql.sexe,
				reg_resultatsql.adreca, reg_resultatsql.ciutat, reg_resultatsql.c_postal, reg_resultatsql.telefon, reg_resultatsql.email,
				reg_resultatsql.num_ss, reg_resultatsql.num_cat, reg_resultatsql.nie, reg_resultatsql.passaport);
            RAISE INFO 'Inserit pacient amb DNI %', reg_resultatsql.dni;
        ELSE
        --  Afegir a la cadena d'errors que el DNI del pacient X del registre numreg del CSV està malament
			ErrorsStr := ErrorsStr || 'Error DNI invàlid: ' || reg_resultatsql.dni || '(corresponent a la fila ' || numreg::text || ' del CSV d''entrada). ';
			RAISE INFO 'Error DNI invàlid per al %', reg_resultatsql.dni;
        END IF;
    END IF;
    DELETE FROM pacients_nous where idpacient=reg_resultatsql.idpacient;
END LOOP;
IF char_length(ErrorsStr)>0 THEN
	Resultat= 'Hi ha hagut algun(s) errors: --> ' || ErrorsStr;
ELSE 
	Resultat='Pacients importats correctament';
END IF;


Return Resultat;
END;
$$
LANGUAGE 'plpgsql';
