CREATE OR REPLACE FUNCTION extreureLletraCentral(cadena text) RETURNS CHAR AS $$

DECLARE
LlargCadena integer;
CaracterCentral integer;
Resultat char;

BEGIN
LlargCadena=char_length(cadena);
CaracterCentral=((LlargCadena+1)/2); --integer division truncates the result
Resultat = substring (cadena from CaracterCentral for 1);
RETURN Resultat;
END

$$ LANGUAGE plpgsql IMMUTABLE;
