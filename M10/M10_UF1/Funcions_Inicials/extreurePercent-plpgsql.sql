CREATE OR REPLACE FUNCTION extreurePercent(cadena text) RETURNS numeric(3,2) AS $$
--Extreure el percentatge a tant per u del final d'una cadena
--ex: 'probabilitat de pluja: 85%/100%
DECLARE
CadenaSensePercent text;
CadenaSensePercentNiNumeros text;
cadenaSenseFrase text;
Llarg_amb_numeros integer;
Llarg_sense_numeros integer;
NumeroString text;
Resultat numeric(3,2);

BEGIN
CadenaSensePercent := left(cadena,char_length(cadena)-1);
RAISE INFO 'CadenaSensePercent: %', CadenaSensePercent;

Llarg_amb_numeros := char_length(CadenaSensePercent);
--trim sirve para cargarte todos los numeros, trailing son los del final
CadenaSensePercentNiNumeros := trim(trailing '0123456789' from CadenaSensePercent);
RAISE INFO 'CadenaSensePercentNiNumeros: %', CadenaSensePercentNiNumeros;

Llarg_sense_numeros := char_length(CadenaSensePercentNiNumeros);
--cogemos la medidas que hay en la cadena sin % los numeros de la diferencia que habia con numeros y sin ex: holaa 25,2-->25
NumeroString := right(CadenaSensePercent, Llarg_amb_numeros-Llarg_sense_numeros);

RAISE INFO 'Numerostring: ''%''', NumeroString;

Resultat = (NumeroString::numeric(5,2) / 100)::numeric(3,2);
--nota: també hi ha el CAST ( expression AS target_type );


RETURN Resultat;
END

$$ LANGUAGE plpgsql IMMUTABLE;
