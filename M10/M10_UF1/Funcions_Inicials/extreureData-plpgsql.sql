CREATE OR REPLACE FUNCTION extreureData(cadena text) RETURNS date AS $$

--Extreu data de cadenes tipus '1-13-19 13:1'
--És a dir: data (amb un o dos dígits per posició) seguida d'espai seguida d'una hora amb un o dos dígits per posició

DECLARE
PosicioEspai integer;
CadenaNomesData text;
Resultat date;
BEGIN

PosicioEspai := position(' ' in cadena);
CadenaNomesData:=left(cadena,PosicioEspai-1);
RAISE INFO 'CadenaNomesData: ''%''', CadenaNomesData;
Resultat:= to_date(CadenaNomesData, 'DD-MM-YY');



RETURN Resultat;
END

$$ LANGUAGE plpgsql IMMUTABLE;
