CREATE OR REPLACE FUNCTION extreureDNI(cadena text) RETURNS varchar(9) AS $$
DECLARE
Resultat varchar(9);

BEGIN
IF char_length(cadena)<9 THEN


Resultat:= 'Error';
ELSE


Resultat:= right(cadena,9);
END IF;

RETURN Resultat;
END

$$ LANGUAGE plpgsql IMMUTABLE;
-------------------------------------------


