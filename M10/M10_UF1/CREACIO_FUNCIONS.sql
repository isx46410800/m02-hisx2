CREACIO DE FUNCIONS

1-
lab_clinic=# create function sumar2(integer,integer) returns integer as 'begin return $1+$2; end;' language 'plpgsql';
CREATE FUNCTION
lab_clinic=# select sumar2(5,2);
 sumar2 
--------
      7
(1 row)

--SE PUEDE USAR ' o $$
--SIEMPRE UTILIZAR COMILLAS SIMPLES EN TEXTOS
2-
lab_clinic=# create or replace function hola(text) 
returns text as $$ begin return 'HOLA '|| $1; end; 
$$ language 'plpgsql';
CREATE FUNCTION
lab_clinic=# select hola('miquel');
    hola     
-------------
 HOLA miquel

3-
	CREATE or replace FUNCTION Llista_Alum() 
	RETURNS setof RECORD
	AS   '
	SELECT * FROM pacients;'
	LANGUAGE SQL;

4-
CREATE OR REPLACE FUNCTION numeros1aN() 
RETURNS RECORD
AS   '
SELECT * FROM prova;'
LANGUAGE SQL;

5-
CREATE OR REPLACE FUNCTION numeros1aN() 
RETURNS setof integer
AS $$ FOR numeros1aN IN 1..i LOOP END LOOP; end;$$
LANGUAGE 'plpgsql';

