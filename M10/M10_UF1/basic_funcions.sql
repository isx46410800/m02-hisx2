-- FUNCIONS BASIQUES

--EXTREURE LA DATA DE LA CADENA DE TEXT "22-15-09 15:05:01" Y 2-1-15 8:5
--Y SALGA 02-01-15

--# EXTREURE DNI DE "Miguel Amoros 46410800" y "Miguel Amoros Moret 46410800C"
--Y SALGA 46410800C

lab_clinic=# select split_part('miguel amoros 46410800C', ' ', 3);
 split_part 
------------
 46410800C
(1 row)

lab_clinic=# select right('miguel amoros 46410800C', 9);
   right   
-----------
 46410800C
(1 row)

FUNCTION

lab_clinic=# create or replace function dni(text) returns text as $$ select right($1, 9);$$ language sql;
CREATE FUNCTION
lab_clinic=# select dni('Miguel amoros 46410800C');
    dni    
-----------
 46410800C
(1 row)


--EXTREURE EL PERCENTATGE DE: "PROBABILITAT DE PLUJA: 87%" Y SALGA 87%

lab_clinic=# select split_part('probabilitat de pluja: 87%', ': ',2);
 split_part 
------------
 87%
(1 row)

FUNCTION
lab_clinic=# create or replace function pluja(text) returns text as $$ select split_part($1, ': ', 2);$$ language sql;
CREATE FUNCTION
lab_clinic=# select pluja('prob de pluja: 87%');
 pluja 
-------
 87%
(1 row)


--EXTREURE EL CARACTER CENTRAL D'UNA CADENA DE TEXT "HOLA QUE TAL? Y SALGA u
lab_clinic=# select substr('hola que tal?', length('hola que tal?')/2, 1);
 substr 
--------
 q
(1 row)

FUNCTION
lab_clinic=# create or replace function mitad(text) returns text as $$ select substr($1, length($1)/2, 1);$$ language sql;
CREATE FUNCTION
lab_clinic=# select mitad('hola que tal');
 mitad 
-------
 q
(1 row)

