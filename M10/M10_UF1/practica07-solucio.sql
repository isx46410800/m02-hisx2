--1) Crear una taula per guardar el nom d'usuari, el timestamp, el nom de taula i l'operació realitzada. Cadascú ha de crear la taula amb l'estructura que consideri més adient

CREATE TABLE auditoria (
    idaudit SERIAL PRIMARY KEY,
    ts TIMESTAMP NOT NULL,
    nom_taula text NOT NULL,
    op char NOT NULL,
    usuari text NOT NULL
);



--2) Crear els trigger i les funcions necessàries per guardar el log dels inserts, dels updates i dels deletes de la taula analítiques i de la taula resultatsanalitiques.

--Opció poc òptima:
CREATE OR REPLACE FUNCTION afegeix_audit() RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        INSERT INTO auditoria VALUES (DEFAULT, CURRENT_TIMESTAMP, TG_TABLE_NAME, 'D', CURRENT_USER);
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO auditoria VALUES (DEFAULT, CURRENT_TIMESTAMP, TG_TABLE_NAME, 'U', CURRENT_USER);
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO auditoria VALUES (DEFAULT, CURRENT_TIMESTAMP, TG_TABLE_NAME, 'I', CURRENT_USER);
        RETURN NEW;
    END IF;
    --Aquí no hauria d'arribar-hi mai:
    RETURN NULL;    
END
$$ LANGUAGE plpgsql;

--Opció òptima:
CREATE OR REPLACE FUNCTION afegeix_audit() RETURNS TRIGGER AS
$$
BEGIN
 
    INSERT INTO auditoria VALUES (DEFAULT, CURRENT_TIMESTAMP, TG_TABLE_NAME, left(TG_OP,1) CURRENT_USER);

    RETURN NULL;    
END
$$ LANGUAGE plpgsql;



CREATE TRIGGER tauditresultats AFTER INSERT OR UPDATE OR DELETE ON resultats FOR EACH ROW EXECUTE PROCEDURE afegeix_audit();
CREATE TRIGGER tauditanalitiques AFTER INSERT OR UPDATE OR DELETE ON analitiques FOR EACH ROW EXECUTE PROCEDURE afegeix_audit();



--3) Treballar amb diferents usuaris per fer les proves.:Crear 3 rols nous amb permisos per lab_clinic. 


--Creem 3 rols que tindran permisos diferents, de més limitat a màxim:
CREATE ROLE lc_consultar NOLOGIN;
CREATE ROLE lc_inserir NOLOGIN;
CREATE ROLE lc_admin NOLOGIN;


--Potser hauríem hagut de crear un SCHEMA ("cluster" de BD) nou (lcschema) per a lab_clinic
-- Si fos així, hauríem de fer també:
GRANT USAGE ON SCHEMA lcschema TO lc_consultar, lc_inserir, lc_admin;
GRANT CREATE ON SCHEMA lcschema TO lc_admin;


GRANT CONNECT ON DATABASE lab_clinic to lc_consultar, lc_inserir, lc_admin;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO lc_consultar;
GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA public TO lc_inserir;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO lc_inserir;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO lc_admin;


CREATE ROLE pepita LOGIN PASSWORD '123456';
CREATE ROLE menganitu LOGIN PASSWORD '123456';
CREATE ROLE zutano LOGIN PASSWORD '123456';

GRANT lc_consultar TO pepita;
GRANT lc_inserir TO menganitu;
GRANT lc_admin TO zutano WITH ADMIN OPTION;


--proves com a pepita (que pertany a lc_consultar)
SELECT * FROM analitiques;
 idanalitica | iddoctor | idpacient |       dataanalitica        
-------------+----------+-----------+----------------------------
           1 |        1 |         2 | 2019-12-01 20:35:43.583018
           2 |        1 |         1 | 2019-12-01 20:35:43.586196
           3 |        2 |         1 | 2019-12-01 20:35:43.588575
(3 rows)

insert into doctors values (DEFAULT, 'Doctor', 'Frankenstein', 'ensamblatge');
ERROR:  permission denied for table doctors
--sí pot crear taules, i afegir files a les *seves* taules




proves com a menganitu:
insert into doctors values (DEFAULT, 'Doctor', 'Frankenstein', 'ensamblatge');
--INSERT 0 1

delete from doctors where iddoctor=4;
--ERROR:  permission denied for table doctors

CREATE TABLE provataula (id serial primary key, numero integer);
--CREATE TABLE


--proves com a zutano:
delete from doctors where iddoctor=4;
--DELETE 1



--de fet, si volem que la resta d'usuaris no puguin crear taules, etc, lo seu és anar amb un schema diferent. Per exemple:

CREATE SCHEMA lcschema AUTHORIZATION postgres;
REVOKE ALL PRIVILEGES ON SCHEMA lcschema FROM public;
GRANT ALL PRIVILEGES ON SCHEMA lcschema TO postgres, lc_admin;
GRANT USAGE ON SCHEMA lcschema TO ROLE lc_consultar, lc_inserir, lc_admin;

--i després podríem continuar amb els GRANTS que hem fet
--The key word PUBLIC refers to the implicitly defined group of all roles.

--faltaria comprovar si els triggers s'activen per a tots els usuaris, o si cal donar permisos especials.
-- veure https://dba.stackexchange.com/questions/46833/what-are-the-privileges-required-to-execute-a-trigger-function-in-postgresql-8-4/46834#46834
