CREATE OR REPLACE FUNCTION dni_correct(dni varchar)
RETURNS text AS
$$
DECLARE
 	resultado varchar := 'False'; --resultado boolean := false
 	numeros varchar;
 	letras_dni varchar := 'TRWAGMYFPDXBNJZSQVHLCKE';
 	resto integer;
 	letra varchar;
 	letra_posicion varchar;
 	numeros_validos bool;
 	letra_valida bool;
BEGIN
 	IF char_length(dni) != 9 THEN
		 resultado := 'LONGITUD INCORRECTA';
	ELSE
 		numeros := substr(dni, 1, 8);
 		numeros_validos := numeros ~ '^[0-9]{8}$';
 		letra := upper(right(dni, 1));
 		letra_valida := letra ~ '^[A-Z]$';
 		if numeros_validos and letra_valida then
			resto := cast(numeros as int) % 23 + 1;
			letra_posicion := substr(letras_dni, resto, 1);
			if letra = letra_posicion THEN
				resultado := TRUE;
			else
				resultado := FALSE;
			end if;
		else
			resultado := 'dni formato incorrecto';
 		end if;
 		
	END IF;
		return resultado;
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;
