CREATE OR REPLACE FUNCTION dni_correct(dni varchar)
RETURNS text AS
$$
DECLARE
 	ret varchar := '1';
 	partnum numeric(8);
BEGIN
 	IF char_length(dni) != 9 THEN
		 ret := 'LONGITUD INCORRECTA';
	ELSE
 		partnum = substr (dni, 1, 8);
		....
		....
	END IF
END	
$$







LANGUAGE 'plpgsql' IMMUTABLE;


CREATE OR REPLACE FUNCTION VALIDANIF  (DNI IN INTEGER, letra   IN OUT CHAR, NIF OUT VARCHAR2)  
RETURN BOOLEAN AS 
   letrasValidas CHAR(23) := 'TRWAGMYFPDXBNJZSQVHLCKE'; 
   letraCorrecta CHAR; 
   letraLeida CHAR := SUBSTR (letra, 1); 
   resto INTEGER; 
BEGIN 
  resto := DNI MOD 23; 
  letraCorrecta := SUBSTR(letrasValidas, resto+1, 1); 
  NIF := letraCorrecta; 
  IF   (letraCorrecta = letraLeida) THEN 
         NIF := TO_CHAR(DNI) || letraLeida ; 
         RETURN true; 
  ELSE 
         letra := letraCorrecta; 
         RETURN false; 
  END IF; 
END VALIDANIF; 
