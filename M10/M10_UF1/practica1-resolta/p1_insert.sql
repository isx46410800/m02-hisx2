--Funcion para insertar pacientes en la BBDD
CREATE OR REPLACE FUNCTION insertar_pacients(int, varchar(15), varchar(30),  varchar(9), date, varchar(1), varchar(50), varchar(30), varchar(10), varchar(9), varchar(30))
RETURNS void AS 
$$
DECLARE
resultado varchar := 'Paciente insertado correctamente'
BEGIN
	insert into pacients (idpacient, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email) 
	values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11); 
END;
$$
LANGUAGE 'plpgsql';
---
/*CREATE TABLE pacients (
  idpacient serial PRIMARY KEY,
  nom varchar(15) NOT NULL,
  cognoms varchar(30) NOT NULL,
  dni varchar(9),
  data_naix date NOT NULL,
  sexe varchar(1) NOT NULL,
  adreca varchar(50) NOT NULL,
  ciutat varchar(30) NOT NULL,
  c_postal varchar(10) NOT NULL,
  telefon varchar(9) NOT NULL,
  email varchar(30) NOT NULL,
  num_ss varchar(12) ,
  num_cat varchar(20) ,
  nie varchar(20),
  passaport varchar(20)*/

select insertar_pacients(3, 'Miguel', 'Amoros Moret', '46410800C', '1993-03-14', 'M', 'C/agudells 66 atico 1', 'Barcelona', '08032', '934071301', 'miguel14amoros@gmail.com');
