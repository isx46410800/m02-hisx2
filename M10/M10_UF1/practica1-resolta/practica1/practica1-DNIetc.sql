CREATE OR REPLACE FUNCTION comprovaDNI(DNI varchar(9)) RETURNS varchar(20) AS $$

DECLARE
 numero integer;
 lletra char;
 resultatBool boolean :=TRUE;
 resultatTxt varchar(100) :='Tot OK!';

BEGIN
 IF character_length(DNI) <> 9 THEN
	resultatBool:=FALSE;
	resultatTxt:='DNI no de 9 caràcters (en té ' || character_length(DNI) || ')';
    RETURN resultatTxt;
 END IF;
 
 numero:=left(DNI,8); --potser no cal?
 lletra:=right(DNI,1);--potser no cal?
 
 resultatTxt:= 'Tenim el número ' || numero || ' i la lletra ' || lletra;
 RETURN resultatTxt;
END;
$$ LANGUAGE plpgsql;
