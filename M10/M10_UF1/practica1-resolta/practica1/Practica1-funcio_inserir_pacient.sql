CREATE OR REPLACE FUNCTION insereix_pacient (nom varchar(15), cognoms varchar(30), dni varchar(9), data_naix date, sexe varchar(1), adreca varchar(50), ciutat varchar(30), c_postal varchar(10), telefon varchar(9), email varchar(30), num_ss varchar(12) , num_cat varchar(20) , nie varchar(20), passaport varchar(20))

RETURNS text AS
$$
DECLARE
    resultat text := 'Pacient inserit correctament';
    
BEGIN
    --Comprovar si el DNI és correcte
    if es_dni_correcte(dni) then
    --Fer l'insert
    INSERT INTO pacients (idpacient, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email, num_ss, num_cat, nie, passaport) VALUES (default, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email, num_ss, num_cat, nie, passaport);
    else
        resultat := 'DNI incorrecte';
    end if;
    return resultat;
    --Gestió d'excepcions:
EXCEPTION
    WHEN unique_violation THEN return 'Violació de clau única'; 
    WHEN not_null_violation THEN return 'Violació de valor que no pot ser nul'; 
    WHEN foreign_key_violation THEN return 'Violació de clau forània'; 
    WHEN others THEN RETURN 'Error no gestionat';
END;
$$
LANGUAGE 'plpgsql';


/*CREATE TABLE pacients (
  idpacient serial PRIMARY KEY,
  nom varchar(15) NOT NULL,
  cognoms varchar(30) NOT NULL,
  dni varchar(9),
  data_naix date NOT NULL,
  sexe varchar(1) NOT NULL,
  adreca varchar(50) NOT NULL,
  ciutat varchar(30) NOT NULL,
  c_postal varchar(10) NOT NULL,
  telefon varchar(9) NOT NULL,
  email varchar(30) NOT NULL,
  num_ss varchar(12) ,
  num_cat varchar(20) ,
  nie varchar(20),
  passaport varchar(20) 
);
*/
