CREATE OR REPLACE FUNCTION es_dni_correcte(dni text)
RETURNS boolean AS
$$
DECLARE
    resultat boolean := false;
    taulaLletres varchar := 'TRWAGMYFPDXBNJZSQVHLCKE';
    numerotxt text;
    numeroint int;
    lletraEntrada char;
    lletraQueTocaria char;
    modul integer;
BEGIN
    IF length(dni) != 9 THEN
		--resultat := 'Longitud incorrecta';
		return false;
    end if;
--a partir d'aquí, dni té 9 caràcters segur!
    
--  RAISE INFO '9 caràcters segur!';

	numerotxt := left (dni, 8);
--	RAISE INFO 'numerotxt val %', numerotxt;
	if not (numerotxt ~ '[0-9]{8}') then
        --resultat := 'Error DNI: els 8 primers caràcters han de ser numèrics';
        return false;
	end if;
	numeroint=numerotxt::integer;
	lletraEntrada := upper(right(dni, 1));
	modul:= numeroint % 23;
	lletraQueTocaria := substr(taulaLletres,modul+1,1);
	IF lletraQueTocaria = lletraEntrada THEN 
		resultat := true; 
    ELSE
        resultat := false;
	END IF;
    RETURN resultat; 
END;
$$
LANGUAGE 'plpgsql' IMMUTABLE;
