        
--    88  
--  ,d88  
-- 88888  
--    88  
--    88  
--    88  
--    88  
--    88  
         


-- 1 Tots els resultats que arribin a la taula de resultats s'han d'evaluar i en el cas que la valoració sigui PATOLÒGIC o PÀNIC s'ha de guardar l'idresultat en una taula que tindrà la següent estructura :

--Crear la taula però bé, amb la FK


CREATE TABLE resultats_patologics( 
idresultat integer NOT NULL, 
stamp timestamp NOT NULL, 
userid text NOT NULL,
idresultat references resultats(idresultat) ON DELETE RESTRICT on UPDATE CASCADE
);  

--Definir funció del trigger
CREATE FUNCTION desa_resultat_patologic() RETURNS trigger AS
$$
DECLARE
	reg_provatecnica provestecnica%ROWTYPE; 
	rnum integer;
BEGIN
   
   
		if NEW.resultats ~ '^[0-9]+(\.[0-9]+)?$' then

			--al registre provestecnica corresponent, verificar que el resultat ha de ser numèric (me'l salto perquè ja hem comprovat si és numèric per la via dels fets)
			--comparar el resultat amb els límits pat (patològic) i pan (pànic) i afegir l'alarma que correspongui, si tal
			SELECT * INTO reg_provatecnica from provestecnica WHERE idprovatecnica=NEW.idprovatecnica;
			rnum=NEW.resultats::integer;
			--El IF ... aquest pressuposa que minpan <= minpat i maxpan>=maxpat
			IF  rnum < reg_provatecnica.minpat OR rnum > reg_provatecnica.maxpat
				INSERT INTO resultats_patologics VALUES (NEW.idresultat, CURRENT_TIMESTAMP, CURRENT_USER);
			END IF;
		END IF;
		return NULL;
END;
$$ 
LANGUAGE plpgsql;

CREATE TRIGGER mirar_resultats_pat AFTER INSERT ON resultats FOR EACH ROW EXECUTE PROCEDURE desa_resultat_patologic();



--.d888b. 
--VP  `8D 
--   odD' 
-- .88'   
--j88.    
--888888D  
         

-- 2 No permetre mai esborrar resultats de la taula resultats, ni analitiques de la taula analitiques.
CREATE FUNCTION bloquejar_delete() RETURNS trigger 
AS $$
   	BEGIN
		RETURN NULL;
		--quan s'invoca en un DELETE, si la funció trigger retorna NULL, s'impedeix; si retorna OLD, s'executa'
		--Impedir que s'esborrin registres sota cap circumstància automàticament implica que la taula recollirà qualsevol error sense possibilitat de correcció...'
	END;
 $$ 
LANGUAGE plpgsql;

--definir els triggers per bloquejar l'esborrar:
CREATE TRIGGER resultats_block_delete BEFORE DELETE ON resultats FOR EACH ROW EXECUTE PROCEDURE bloquejar_delete();

CREATE TRIGGER analitiques_block_delete BEFORE DELETE ON analitiques FOR EACH ROW EXECUTE PROCEDURE bloquejar_delete();


-- ad888888b,  
--d8"     "88  
--        a8P  
--     aad8"   
--     ""Y8,   
--        "8b  
--Y8,     a88  
-- "Y888888P'  



-- 3 Quan totes les proves d'una analítica tinguin resultat i no existeixi cap resultat d'aquesta analítica a la taula de resultats_patologics, es donarà l'analítica per acabada i es podrà fer l'informe definitiu. Per això guardarem l'id de l'analítica a la taula informes:

CREATE TABLE informes(
idanalitica integer NOT NULL,
stamp timestamp NOT NULL); 

--Això vol dir crear un trigger per cada cop que s'insereix a resultats el resultat d'una prova, per veure si compleix les condicions i es fa l'insert a la taula d'informes. De totes maneres: com sabem quan s'han fet totes les proves d'una analítica, si la taula provestecnica no diu a quina analítica pertanyen?
--	Possible solució: afegir un camp a provestecnica la idanalitica que l'ha generat i així podem saber si hi ha un resultat per a cada provestecnica d'una analitica concreta.





--        ,d8    
--      ,d888    
--    ,d8" 88    
--  ,d8"   88    
--,d8"     88    
--8888888888888  
--         88    
--         88  
--4. Cal guardar a la taula alarmes les dades necessàries per notificar a les autoritats sanitàries dels resultats patològics de les proves configurades per donar alarma. Desenvolupar o modificar els triggers i les funcions que calgui per guardar les alarmes.

--Concretament cal fer:
	-- trigger per quan apareix un resultat: comprovar respecte la idprovestecnica relacionada el nivell d'alarma i si cal, crear l'entrada a alarmes (la pràctica 3 ajuda en això)

CREATE FUNCTION revisar_resultat() RETURNS trigger 
AS $$
	DECLARE
	reg_provatecnica provestecnica%ROWTYPE; 
	rnum integer;
	nivell_alarma integer;
	missatge_alarma text;
   	BEGIN
		--verificar que és un número (una o més xifres de número, opcionalment seguides de (punt i una o més xifres
		if NEW.resultats ~ '^[0-9]+(\.[0-9]+)?$' then


			--al registre provestecnica corresponent, verificar que el resultat ha de ser numèric (me'l salto perquè ja hem comprovat si és numèric per la via dels fets)
			--comparar el resultat amb els límits pat (patològic) i pan (pànic) i afegir l'alarma que correspongui, si tal
			SELECT * INTO reg_provatecnica from provestecnica WHERE idprovatecnica=NEW.idprovatecnica;
			rnum=NEW.resultats::integer;
			--El IF ... ELSE IF aquest pressuposa que minpan <= minpat i maxpan>=maxpat
			IF renum > reg_provatecnica.maxpan OR rnum < reg_provatecnica.minpan THEN
				--alarma pànic. Serà codi 2 segons la pràctica 3
				nivell_alarma:=2;
				missatge_alarma:='Pànic'
			ELSE IF renum > reg_provatecnica.maxpat OR rnum < reg_provatecnica.minpat
				nivell_alarma:=1;
				missatge_alarma:='Patològic'
			END IF;
			INSERT INTO alarmes VALUES (default, NEW.idresultat, nivell_alarma, false, missatge_alarma)

		end if;
		return NULL;
	END;
 $$ 
LANGUAGE plpgsql;

CREATE TRIGGER trig_revisar_resultat AFTER INSERT ON resultats FOR EACH ROW EXECUTE PROCEDURE revisar_resultat();
