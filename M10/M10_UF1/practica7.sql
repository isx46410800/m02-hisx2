--PRACTICA 7

--CREAR TABLA PARA GUARDAR LAS OPERACIONES HECHAS POR UN USER
--almacena la operacion, fecha/hora, el usuario y nombre de la tabla
create table auditoria (
	operacio varchar(1) not null,
	data_hora timestamp not null,
	nom_user varchar(100) not null,
	nom_table varchar(100) not null
);

--FUNCION PARA GUARDAR LOS LOGS DE LOS INSERTS/DELETES/UPDATES
--current_time: fecha actual del sistema
--current_user: usuario que realiza la operacion en ese momento
--TG_TABLE_NAME: te dice la tabla en la que haces la operacion, reacciona al triger/funcion
CREATE or REPLACE FUNCTION logs_auditoria() RETURNS trigger AS
$$
BEGIN
IF (TG_OP = 'DELETE') THEN
INSERT INTO auditoria values ('D', current_timestamp, current_user, TG_TABLE_NAME);
RETURN OLD;
ELSEIF (TG_OP = 'UPDATE') THEN
INSERT INTO auditoria values ('U', current_timestamp, current_user, TG_TABLE_NAME);
RETURN NEW;
ELSEIF (TG_OP = 'INSERT') THEN
INSERT INTO auditoria values ('I', current_timestamp, current_user, TG_TABLE_NAME);
RETURN NEW;
END IF;
RETURN NULL;
END;
$$
LANGUAGE PLPGSQL;

--triggers para las tablas analitiques y resultats
CREATE TRIGGER auditar_analitiques AFTER INSERT OR UPDATE OR DELETE ON analitiques FOR EACH ROW EXECUTE PROCEDURE logs_auditoria();
CREATE TRIGGER auditar_resultats AFTER INSERT OR UPDATE OR DELETE ON resultats FOR EACH ROW EXECUTE PROCEDURE logs_auditoria();

--COMPROBACION
lab_clinic=# insert into analitiques (iddoctor, idpacient, dataanalitica)values (2,1, current_timestamp);
INSERT 0 1
lab_clinic=# delete from analitiques where idanalitica = 3;
DELETE 1
lab_clinic=# update analitiques set iddoctor=3 where idpacient=2;
UPDATE 1

lab_clinic=# select * from auditoria;
 operacio |         data_hora          | nom_user |  nom_table  
----------+----------------------------+----------+-------------
 I        | 2020-01-13 11:38:43.653605 | postgres | analitiques
 D        | 2020-01-13 11:44:15.18338  | postgres | analitiques
 U        | 2020-01-13 11:45:59.58652  | postgres | analitiques
(3 rows)

--crear roles
CREATE ROLE lc_consultar NOLOGIN;
CREATE ROLE lc_inserir NOLOGIN;
CREATE ROLE lc_admin NOLOGIN;

---
--CREACION DE UN SCHEMA
CREATE SCHEMA lcschema AUTHORIZATION postgres;
--TODOS LOS SCHEMAS SE CREAN EN PUBLIC (PUBLIC-POSTGRES OWNER)
REVOKE ALL PRIVILEGES ON SCHEMA lschema FROM public;
-- todos los privilegios para los users:
GRANT ALL PRIVILEGES ON SCHEMA lschema TO postgres, lc_admin;
--privilegios para usar este schema para:
GRANT USAGE ON SCHEMA lschema TO ROLE lc_consultar, lc_inserir, lc_admin;

--s'hauria de crear un SCHEMA de lab_clinic i també fer aixo;
GRANT USAGE ON SCHEMA lschema to lc_consultar, lc_inserir, lc_admin;
GRANT CREATE ON SCHEMA lschema to lc_admin;

--DAR LOS PRIVILEGIOS A CIERTOS USUARIOS
---para poder conectarse a la bbdd
GRANT CONNECT ON DATABASE lab_clinic to lschema to lc_consultar, lc_inserir, lc_admin;
---para ver las tablas, y podra crear tablas y filas a su nombre
GRANT SELECT ON ALL TABLES IN SCHEMA public TO lc_consultar;
---para poder ver y insertar (no borrar)
GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA public to lc_inserir;
---para poder insertar, tambien necesita permisos de inserir secuencias
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to lc_inserir;
---para tener todos los privilegios en la bbdd
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to lc_admin;



--creamos usuarios, para poder tener roles se necesita LOGIN con create role
postgres=# create user miguel password 'miguel';
CREATE ROLE
--create role miguel login password 'miguel';
postgres=# create user walid password 'walid';
CREATE ROLE

--asignar los roles a los usuarios
GRANT lc_consultar to walid;
GRANT lc_inserir to miguel;
GRANT lc_admin to miguel with admin option;



--permiso de superusuario
postgres=# alter role miguel with superuser;
ALTER ROLE
--todos los privilegios en esta base de datos
postgres=# grant all privileges on database lab_clinic to walid;
GRANT

--entrar a la bbdd como miguel
[isx46410800@i05 ipc2019]$ su postgres
Password: 
bash-4.4$ psql lab_clinic miguel;
Password for user miguel: 
psql (9.6.10)

lab_clinic=# insert into analitiques (iddoctor, idpacient, dataanalitica)values (2,1, current_timestamp);
INSERT 0 1
lab_clinic=# select * from auditoria;
 operacio |         data_hora          | nom_user |  nom_table  
----------+----------------------------+----------+-------------
 I        | 2020-01-13 11:38:43.653605 | postgres | analitiques
 D        | 2020-01-13 11:44:15.18338  | postgres | analitiques
 U        | 2020-01-13 11:45:59.58652  | postgres | analitiques
 I        | 2020-01-13 12:05:08.948816 | miguel   | analitiques
(4 rows)


