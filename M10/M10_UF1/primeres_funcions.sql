CREATE FUNCTION suma (INTEGER,INTEGER)
RETURNS INTEGER
AS $$
BEGIN
RETURN $1+$2;
END;
$$ LANGUAGE 'plpgsql';

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION hola (TEXT)
RETURNS TEXT
AS $$
BEGIN
RETURN 'HOLA '|| $1;
END;
$$ LANGUAGE 'plpgsql';

--------------------------------------------------------------------------------

CREATE FUNCTION INS_ALUM()
RETURNS VOID
AS $$
INSERT INTO prova(a,b) VALUES (100,200);
$$ LANGUAGE sql;

--------------------------------------------------------------------------------

CREATE FUNCTION llista_Alum()
RETURNS RECORD   -- RECORD DEVUELVE UNA LISTA CON CAMPOS
AS $$            -- SI QUEREMOS QUE DEVUELVA MÁS "RETURNS SETOF RECORD"
SELECT * FROM prova;
$$ LANGUAGE sql;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION counter ()
RETURNS SETOF INTEGER
AS $$
BEGIN
   FOR counter IN 1..10 LOOP
   RAISE NOTICE 'Counter: %', counter;
   END LOOP;
END;
$$ LANGUAGE 'plpgsql';
--------------------------------------------------------------------------------
