create table comarques (
		id_comarca serial primary key,
		comarca varchar(100) not null,
		superficie int not null,
		habitants int not null,
		unique(comarca)
	);

create table casa_colonies (
		id_casa serial primary key,
		nom_casa varchar(200) not null,
		capacitat int not null,
		id_comarca int not null,
		foreign key (id_comarca) references comarques(id_comarca)
	);


create table nens (
		id_nen serial primary key,
		nom_nen varchar(100) not null,
		cognom_nen varchar(200) not null,
		id_casa int not null,
		id_comarca int not null,
		foreign key (id_casa) references casa_colonies(id_casa),
		foreign key (id_comarca) references comarques(id_comarca)
	);
	
create table activitats (
		id_activitat serial primary key,
		activitat varchar(200) not null
	);

create table practiques (
		id_practica serial primary key,
		id_casa int not null,
		id_activitat int not null,
		nota int not null,
		foreign key (id_casa) references casa_colonies(id_casa),
		foreign key (id_activitat) references activitats(id_activitat),
		unique(id_casa, id_activitat)
);

>INSERTS>

COMARQUES
practica3=# insert into comarques (comarca, superficie, habitants) values ('Barcelones', 5250000, 2000000);
INSERT 0 1
practica3=# insert into comarques (comarca, superficie, habitants) values ('Tarragones', 4200000, 1500000);
INSERT 0 1
practica3=# select * from comarques;
 id_comarca |  comarca   | superficie | habitants 
------------+------------+------------+-----------
          1 | Barcelones |    5250000 |   2000000
          2 | Tarragones |    4200000 |   1500000
(2 rows)

CASES
practica3=# insert into casa_colonies (nom_casa, capacitat, id_comarca) values ('xaloc', 300, 2);
INSERT 0 1
practica3=# insert into casa_colonies (nom_casa, capacitat, id_comarca) values ('hispania', 155, 1);
INSERT 0 1
practica3=# select * from casa_colonies;
 id_casa | nom_casa | capacitat | id_comarca 
---------+----------+-----------+------------
       1 | xaloc    |       300 |          2
       2 | hispania |       155 |          1
(2 rows)

NENS
practica3=# insert into nens (nom_nen, cognom_nen, id_casa, id_comarca) values ('Miguel', 'Amoros', 2, 2);
INSERT 0 1
practica3=# insert into nens (nom_nen, cognom_nen, id_casa, id_comarca) values ('Jordi', 'Quiros', 1, 1);
INSERT 0 1
practica3=# SELECT * FROM nens;
 id_nen | nom_nen | cognom_nen | id_casa | id_comarca 
--------+---------+------------+---------+------------
      2 | Miguel  | Amoros     |       2 |          2
      3 | Jordi   | Quiros     |       1 |          1
(2 rows)


ACTIVITATS
practica3=# insert into activitats (activitat) values ('futbol');
INSERT 0 1
practica3=# insert into activitats (activitat) values ('basket');
INSERT 0 1
practica3=# select * from activitats;
 id_activitat | activitat 
--------------+-----------
            1 | futbol
            2 | basket
(2 rows)

PRACTIQUES
practica3=# insert into practiques (id_casa, id_activitat, nota) values (1, 1, 7);
INSERT 0 1
practica3=# insert into practiques (id_casa, id_activitat, nota) values (2, 1, 9);
INSERT 0 1
practica3=# insert into practiques (id_casa, id_activitat, nota) values (1, 2, 5);
INSERT 0 1
practica3=# insert into practiques (id_casa, id_activitat, nota) values (2, 2, 6);
INSERT 0 1
practica3=# select * from practiques;
 id_practica | id_casa | id_activitat | nota 
-------------+---------+--------------+------
           2 |       1 |            1 |    7
           3 |       2 |            1 |    9
           4 |       1 |            2 |    5
           5 |       2 |            2 |    6
(4 rows)

<ERRORES>

COMARQUES
practica3=# insert into comarques (comarca, superficie, habitants) values (Barcelones, 5250000, 2000000);
ERROR:  no existe la columna «barcelones»
LINE 1: ...omarques (comarca, superficie, habitants) values (Barcelones...

CASAS
practica3=# insert into casa_colonies (nom_casa, capacitat, id_comarca) values ('xaloc', 300, 2);
ERROR:  inserción o actualización en la tabla «casa_colonies» viola la llave foránea «casa_colonies_id_comarca_fkey»
DETAIL:  La llave (id_comarca)=(2) no está presente en la tabla «comarques».
practica3=# insert into casa_colonies (nom_casa, capacitat, id_comarca) values (Hilton, 590, 1);
ERROR:  no existe la columna «hilton»
LINE 1: ...olonies (nom_casa, capacitat, id_comarca) values (Hilton, 59...

NENS
practica3=# insert into nens (nom_nen, cognom_nen, id_casa, id_comarca) values (Pepe, 'Gallardo', 1, 1);
ERROR:  no existe la columna «pepe»
LINE 1: ...nom_nen, cognom_nen, id_casa, id_comarca) values (Pepe, 'Gal...'
                                                             ^
practica3=# insert into nens (nom_nen, cognom_nen, id_casa, id_comarca) values ('Pepe', 'Gallardo', 3, 3);
ERROR:  inserción o actualización en la tabla «nens» viola la llave foránea «nens_id_casa_fkey»
DETAIL:  La llave (id_casa)=(3) no está presente en la tabla «casa_colonies».

ACTIVITATS
practica3=# insert into activitats (activitat) values (tenis);
ERROR:  no existe la columna «tenis»
LINE 1: insert into activitats (activitat) values (tenis);

PRACTIQUES
practica3=# insert into practiques (id_casa, id_activitat, nota) values (3, 2, 6);
ERROR:  inserción o actualización en la tabla «practiques» viola la llave foránea «practiques_id_casa_fkey»
DETAIL:  La llave (id_casa)=(3) no está presente en la tabla «casa_colonies».
practica3=# insert into practiques (id_casa, id_activitat, nota) values (2, 3, 6);
ERROR:  inserción o actualización en la tabla «practiques» viola la llave foránea «practiques_id_activitat_fkey»
DETAIL:  La llave (id_activitat)=(3) no está presente en la tabla «activitats».
practica3=# insert into practiques (id_casa, id_activitat, nota) values (2, 3, SEIS);
ERROR:  no existe la columna «seis»
LINE 1: ...ractiques (id_casa, id_activitat, nota) values (2, 3, SEIS);





